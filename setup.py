from setuptools import setup

dependencies = [
    "blspy==1.0.5",  # Signature library
    "chiavdf==1.0.2",  # timelord and vdf verification
    "chiabip158==1.0",  # bip158-style wallet filters
    "chiapos==1.0.4",  # proof of space
    "clvm==0.9.7",
    "clvm_rs==0.1.8",
    "clvm_tools==0.4.3",
    "aiohttp",  # HTTP server for full node rpc
    "aiosqlite==0.17.0",  # asyncio wrapper for sqlite, to store blocks
    "bitstring==3.1.7",  # Binary data management library
    "colorlog==5.0.1",  # Adds color to logs
    "concurrent-log-handler==0.9.19",  # Concurrently log and rotate logs
    "cryptography==3.4.7",  # Python cryptography library for TLS - keyring conflict
    "keyring==23.0.1",  # Store keys in MacOS Keychain, Windows Credential Locker
    "keyrings.cryptfile==1.3.4",  # Secure storage for keys on Linux (Will be replaced)
    #  "keyrings.cryptfile==1.3.8",  # Secure storage for keys on Linux (Will be replaced)
    #  See https://github.com/frispete/keyrings.cryptfile/issues/15
    "PyYAML==5.4.1",  # Used for config file format
    "setproctitle==1.2.2",  # Gives the chia processes readable names
    "sortedcontainers==2.3.0",  # For maintaining sorted mempools
    "websockets",  # For use in wallet RPC and electron UI
    "click==7.1.2",  # For the CLI
    "dnspython==2.1.0",  # Query DNS seeds
    "eth_keyfile",
    "scrypt",
    "pycryptodome",
    "web3",
    "py-solc-x",
]

upnp_dependencies = [
    "miniupnpc==2.2.2",  # Allows users to open ports on their router
]

dev_dependencies = [
    "pytest",
    "pytest-asyncio",
    "flake8",
    "mypy",
    "black",
    "aiohttp_cors",  # For blackd
    "ipython",  # For asyncio debugging
]

kwargs = dict(
    name="nbzz",
    author="anonymous",
    author_email="anonymous@nbzz.net",
    description="Nbzz blockchain full node, farmer, timelord, and wallet.",
    url="https://nbzz.net/",
    license="Apache License",
    python_requires=">=3.8, <4",
    keywords="nbzz blockchain node",
    install_requires=dependencies,
    setup_requires=["setuptools_scm"],
    extras_require=dict(
        uvloop=["uvloop"],
        dev=dev_dependencies,
        upnp=upnp_dependencies,
    ),
    packages=[
        "nbzz",
    ],
    entry_points={
        "console_scripts": [
            "nbzz = nbzz.cmds.nbzz:main",
            "nbzz_wallet = nbzz.server.start_wallet:main",
            "nbzz_full_node = nbzz.server.start_full_node:main",
            "nbzz_harvester = nbzz.server.start_harvester:main",
            "nbzz_farmer = nbzz.server.start_farmer:main",
            "nbzz_introducer = nbzz.server.start_introducer:main",
            "nbzz_timelord = nbzz.server.start_timelord:main",
            "nbzz_timelord_launcher = nbzz.timelord.timelord_launcher:main",
            "nbzz_full_node_simulator = nbzz.simulator.start_simulator:main",
        ]
    },
    package_data={
        "nbzz": ["pyinstaller.spec"],
        "": ["*.clvm", "*.clvm.hex", "*.clib", "*.clinc", "*.clsp"],
        "nbzz.util": ["initial-*.yaml", "english.txt"],
        "nbzz.ssl": ["nbzz_ca.crt", "nbzz_ca.key", "dst_root_ca.pem"],
    },
    use_scm_version={"fallback_version": "unknown-no-.git-directory"},
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    zip_safe=False,
)


if __name__ == "__main__":
    setup(**kwargs)
